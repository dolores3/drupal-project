const {src, dest, watch} = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');

/**
 * Compile sass.
 *
 * @param options
 *   Sass options.
 */
function compile(options) {
  return source(options)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 2 versions'))
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(dest('css', { sourcemaps: 'maps' }))
    .pipe(browserSync.stream());
}

/**
 * Loads sass source.
 *
 * @param options
 *   Sass options.
 */
function source(options = {}) {
  return src('sass/style.scss', options);
}

/**
 * Compile with source maps.
 */
function maps() {
  return compile({ sourcemaps: true });
}

/**
 * Watch and compile with source maps.
 *
 * @param cb
 *   The callback function.
 */
function watcher(cb) {
  maps();
  watch('sass/**/*.{scss,sass}', maps, cb);
}

/**
 * Initialize browser sync.
 */
function sync() {
  browserSync.init({
    proxy: "drupal-starter.test",
  });
  watcher();
}

exports.compile = compile
exports.watch = watcher
exports.sync = sync
exports.maps = maps

exports.default = watcher
