<?php

namespace Drush\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\AnnotatedCommand\Hooks\HookManager;
use Drush\Attributes as CLI;
use Drush\Commands\core\RsyncCommands;
use Drush\Commands\sql\SqlSyncCommands;
use Exception;
use Symfony\Component\Console\Input\InputInterface;
use Consolidation\AnnotatedCommand\AnnotationData;

/**
 * [WARNING] Don't change unless you know what you are doing.
 */
class PolicyCommands extends DrushCommands {

  /**
   * Array with commands that are whitelisted.
   * [WARNING] Don't change unless you know what you are doing.
   *
   * @var array|string[]
   */
  protected array $whiteListedCommands = [
    'atom_stm:module',
    'atom_stm:module:test_connection',
    'cache:rebuild',
    'config:get',
    'core:status',
    'list',
    'sql:dump', // Needed for 'phing sanitized-db-sync'
    'user:login',
  ];

  /**
   * Array With the Whitelisted IP's.
   * [WARNING] Don't change unless you know what you are doing.
   *
   * @var array|string[]
   */
  protected array $ipWhitelist = [
    '93.186.178.41', // Pre-Atom
    '93.191.132.96', // Dev 1
    '37.97.148.148', // Dev 2
  ];

  /**
   * Prevent catastrophic braino. Note that this file has to be local to the
   * machine that initiates the sql:sync command.
   *
   * @throws \Exception
   */
  #[CLI\Hook(type: HookManager::ARGUMENT_VALIDATOR, target: SqlSyncCommands::SYNC)]
  public function sqlSyncValidate(CommandData $commandData): void {
    if (str_starts_with($commandData->input()->getArgument('target'), '@prod')) {
      throw new Exception(dt('Per !file, you may never overwrite the production database.', ['!file' => __FILE__]));
    }
  }

  /**
   * Limit rsync operations to production site.
   *
   * @throws \Exception
   */
  #[CLI\Hook(type: HookManager::ARGUMENT_VALIDATOR, target: RsyncCommands::RSYNC)]
  public function rsyncValidate(CommandData $commandData): void {
    if (str_starts_with($commandData->input()->getArgument('target'), '@prod')) {
      throw new Exception(dt('Per !file, you may never rsync to the production site.', ['!file' => __FILE__]));
    }
  }

  /**
   * Prevent catastrophic braino. Check all the commands and see if
   * the command can be run based on the remote-host.
   *
   * @hook pre-init *
   *
   * @throws \Exception
   */
  public function commandValidate(InputInterface $input, AnnotationData $annotationData): void {
    $aliasConfig = $this->getConfig()->getContext('alias');

    // If there is no remote host set, or the command can not be found,
    // stop the validation.
    if (
      !$aliasConfig->has('options') === TRUE
      || !is_array($aliasConfig->get('options'))
      || !isset($aliasConfig->get('options')['remote-host'])
      || $annotationData->has('command') === FALSE
    ) {
      return;
    }

    // Get the Host of the config.
    $host = $aliasConfig->get('options')['remote-host'];

    // If the host is whitelisted, do not check any commands.
    if (in_array($host, $this->ipWhitelist, TRUE)) {
      return;
    }

    // Get the executed command.
    $command = $annotationData->get('command');

    if (!in_array($command, $this->whiteListedCommands, TRUE)) {
      throw new Exception(dt('Per !file, you may not use !command on !target.', [
        '!file' => __FILE__,
        '!command' => $command,
        '!target' => $aliasConfig->get('options')['uri'],
      ]));
    }
  }
}
